import '@babel/polyfill';
import app from './config/server';
import routes from './config/routes';

const main = async () => {
    await app.listen(app.get('port'));
    await routes(app);
    console.log(`Server running on http://localhost:${app.get('port')}/`);
};

main();
