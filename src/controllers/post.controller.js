import Post from "../models/Post";

export const index = async ({user: {_id}}, res) => {
    try {
        return res.json(await Post.find({user: _id}).sort({dateRegister: -1}));
    } catch (e) {
        return res.status(500).json({
            message: "Something goes wrong.",
        });
    }
};

export const store = async ({body: {title, image_url, content}, user: {_id}}, res) => {
    try {
        const newPost = await Post.create({
            title,
            image_url,
            content,
            user: _id
        });

        return res.json({
            message: "Created successfully.",
            post: newPost
        });
    } catch ({errors}) {
        return res.status(404).json(errors);
    }
};

export const find = async ({params: {search}, user: {_id}}, res) => {
    try {
        const post = await Post.findOne({
            user: _id,
            $or: [
                {title: search},
                {content: search}
            ]
        });

        if (post) {
            return res.json(post);
        }

        return res.status(404).json({
            message: "Post not found."
        });
    } catch (e) {
        return res.status(404).json(e);
    }
};

export const destroy = async ({params}, res) => {
    try {
        const {id} = params;
        const {deletedCount} = await Post.deleteOne({_id: id});

        if (deletedCount > 0) {
            return res.json({
                message: "Post deleted successfully."
            });
        }

        return res.json({
            message: "Post not found."
        });
    } catch (e) {
        return res.status(404).json(e);
    }
};
