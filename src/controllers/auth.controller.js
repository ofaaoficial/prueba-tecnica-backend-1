import User from "../models/User";
import {encrypt, verifyPassword} from "../helpers/auth";
import jwt from 'jsonwebtoken';

export const register = async ({body: {username, email, password}}, res) => {
    try {
        let encryptedPassword = await encrypt(password);
        const newUser = await User.create({
            username,
            email,
            password: encryptedPassword
        });

        const token = jwt.sign({
            id: newUser.id,
            email: newUser.email
        }, process.env.SECRET, {
            expiresIn: "1h"
        });

        return res.status(201).json({
            message: "Registered user successfully.",
            token,
            username
        });
    } catch ({errors}) {
        return res.status(404).json(errors);
    }
};

export const login = async ({body}, res) => {
    try {
        const {email, password} = body;

        const user = await User.findOne({email});

        if (user && await verifyPassword(password, user.password)) {

            const token = jwt.sign({
                id: user.id,
                email: user.email
            }, process.env.SECRET, {
                expiresIn: "1h"
            });

            return res.status(200).json({
                token,
                username: user.username
            });
        }

        return res.status(401).json({
            message: 'Incorrect password.'
        });
    } catch ({errors}) {
        return res.status(404).json(errors);
    }
}
