import {hash, compare, genSalt} from 'bcryptjs';
import jwt from "jsonwebtoken";
import User from "../models/User";

export const encrypt = async (string) => {
    return await hash(string, await genSalt(10));
};

export const verifyPassword = async (string, hash) => {
    return await compare(string, hash);
};

export const verifyToken = async (req, res, next) => {
    try {
        const token = req.headers['x-access-token'];

        if (!token) {
            return res.status(401).json({message: 'No token provided.'});
        }

        const {email} = jwt.verify(token, process.env.SECRET);

        const user = await User.findOne({email});

        if (!user) {
            return res.status(404).json({message: 'User not found.'})
        }

        req.user = user;
        return next();
    } catch (e) {
        return res.status(401).json({
            message: 'Incorrect token.'
        })
    }
}
