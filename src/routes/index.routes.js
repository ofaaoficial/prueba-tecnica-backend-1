import {Router} from 'express';
import {index} from "../controllers/index.controller";

const routes = Router();

routes.get('/', index);

export default routes;
