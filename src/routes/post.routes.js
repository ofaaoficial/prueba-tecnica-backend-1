import {Router} from 'express';
import {verifyToken} from "../helpers/auth";
import {index, store, find, destroy} from "../controllers/post.controller";

const routes = Router();

routes.use(verifyToken);

routes.route('/post')
    .get(index)
    .post(store);

routes.delete('/post/:id', destroy);
routes.get('/post/:search', find);

export default routes;
