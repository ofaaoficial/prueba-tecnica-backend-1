import mongoose from 'mongoose';
import {config} from "dotenv";

// Dotenv
config();

mongoose.connect(process.env.URL_MONGO_DB, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
})
    .then(() => console.log('DB is connected'))
    .catch(console.log);
