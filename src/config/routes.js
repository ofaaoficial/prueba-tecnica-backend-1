import index from './../routes/index.routes';
import auth from './../routes/auth.routes';
import post from './../routes/post.routes';

const routes = async (app) => {
    app.use(index);
    app.use('/api', auth);
    app.use('/api', post);
};

export default routes;

