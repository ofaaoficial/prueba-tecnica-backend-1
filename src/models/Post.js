import {Schema, model} from 'mongoose';

const PostSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    image_url: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    user: {
      type: Schema.ObjectId,
      ref: 'User',
      required: true
    },
    dateRegister: {
        type: Date,
        required: true,
        default: Date.now
    }
});

export default model('Post', PostSchema);
