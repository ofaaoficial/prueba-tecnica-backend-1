# Prueba Técnica
Solución de la prueba técnica.
## Rutas API
### Autenticación
| Función | Método | URL |
| :---: | :---: | :---: |
| Registro | POST | `/api/register` |
| Ingresar | POST | `/api/login` |
### Noticias
| Función | Método | URL | Encabezado requerido |
| :---: | :---: | :---: | :---: |
| Crear | POST | `/api/post` | x-access-token |
| Listar | GET | `/api/post` | x-access-token |
| Borrar | DELETE | `/api/post/:id` | x-access-token |
| Filtrar | GET | `/api/post/:search` | x-access-token |

Se debe enviar en el encabezado de la petición un variable `x-access-token` con el valor del token recibido en el punto de acceso de `/api/login`.

**URL en Heroku: https://backend-prueba-tecnica.herokuapp.com/**

## Licencia 🔥
Copyright © 2020-present [Oscar Amado](https://github.com/ofaaoficial) 🧔
